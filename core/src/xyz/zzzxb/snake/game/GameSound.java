package xyz.zzzxb.snake.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * @author Zzzxb  2019/10/13 16:03
 * @description:
 */
public class GameSound {
    private static Sound eatFoodSound;
    private static Music gameOverMusic;
    private static Music backgroundMusic;

    public GameSound() {
        eatFoodSound = Gdx.audio.newSound(Gdx.files.internal("sound/gold.mp3"));
        gameOverMusic = Gdx.audio.newMusic(Gdx.files.internal("music/game-over.mp3"));
        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("music/evening-breeze.mp3"));
    }

    public static Sound getEatFoodSound() {
        return eatFoodSound;
    }

    public static Music getBackgroundMusic() {
        return backgroundMusic;
    }

    public static Music getGameOverMusic() {
        return gameOverMusic;
    }

    public static  void dispose() {
        eatFoodSound.dispose();
        gameOverMusic.dispose();
        gameOverMusic.dispose();
    }
}
