package xyz.zzzxb.snake.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * @author Zzzxb  2019/10/13 14:13
 * @description:
 */
public class Food {
    private int blockSize = 16;
    private int goldNum;
    private int[] xyFood; // 暂时无法调整食物数量
    private Pixmap pixmap;
    private Texture texture;

    public Food() {
        pixmap = new Pixmap(blockSize, blockSize, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.GOLD);
        pixmap.fillRectangle(0, 0, blockSize, blockSize);
        texture = new Texture(pixmap);
        init();
    }

    public void init() {
        goldNum = 1;
        xyFood = new int[2];
        createFood();
    }

    public void createFood() {
        xyFood[0] = (int)(Math.random()*30+1) * blockSize;
        xyFood[1] = (int)(Math.random()*30+1) * blockSize;
    }

    public void draw(Batch batch) {
        batch.draw(texture, xyFood[0], xyFood[1]);
    }

    public int[] getXyFood() {
        return xyFood;
    }

    public void dispose() {
        texture.dispose();
        pixmap.dispose();
    }
}
