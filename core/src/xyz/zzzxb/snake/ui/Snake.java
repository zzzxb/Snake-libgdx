package xyz.zzzxb.snake.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import xyz.zzzxb.snake.game.GameSound;
import xyz.zzzxb.snake.utils.Direction;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Zzzxb  2019/10/12 21:32
 * @description:
 */
public class Snake {
    private int blockSize = 16;
    private float speed;
    private Color color;
    private Pixmap pixmap;
    private Texture texture;
    private Direction direction;
    private boolean keyBoardState = true;
    private List<int[]> xyList;
    private int[] xy;
    private int gold;

    public Snake() {
        color = Color.DARK_GRAY;
        pixmap = new Pixmap(blockSize, blockSize, Pixmap.Format.RGBA8888);
        xyList = new ArrayList();
        pixmap.setColor(color);
        pixmap.fillRectangle(0, 0, blockSize, blockSize);
        texture = new Texture(pixmap);
        init();
    }

    public void init() {
        int initX = Gdx.graphics.getWidth();
        int initY = Gdx.graphics.getHeight();
        gold = 0;
        speed = 0.06f;
        xyList.clear();
        xy = new int[]{initX/2,initY/2};
        xyList.add(new int[]{initX/2-blockSize,initY/2});
        xyList.add(new int[]{ xy[0], xy[1]});
        direction = Direction.stop;
    }

    public void draw(Batch batch) {
        for (int[] ints : xyList) {
            batch.draw(texture, ints[0], ints[1]);
        }
    }

    public void gameOver() {
        if ( xy[0] < blockSize || xy[0] > Gdx.graphics.getWidth()- blockSize*2
                || xy[1] < blockSize || xy[1] > Gdx.graphics.getHeight() - blockSize*2) {
            direction = Direction.over;
            gameOverMusic();
            return;
        }
        for (int i = 0; i < xyList.size()-1; i++) {
            int[] xy = xyList.get(i);
            if (this.xy[0] == xy[0] && this.xy[1] == xy[1]) {
                direction = Direction.over;
                gameOverMusic();
                return;
            }
        }
    }

    public void gameOverMusic() {
        GameSound.getEatFoodSound().stop();
        GameSound.getBackgroundMusic().stop();
        GameSound.getGameOverMusic().play();
    }

    public void move() {
        if (direction.equals(Direction.up)) {
            xy[1] += blockSize;
            return;
        }
        if (direction.equals(Direction.down)) {
            xy[1] -= blockSize;
            return;
        }
        if (direction.equals(Direction.left)) {
            xy[0] -= blockSize;
            return;
        }
        if (direction.equals(Direction.right)) {
            xy[0] += blockSize;
            return;
        }
    }

    private float count = 0; // 计数器
    public void controller(float deltaTime){
        count += deltaTime;

        if (Gdx.input.isKeyPressed(Input.Keys.MINUS) && keyBoardState) {
            keyBoardState = false;
            alterSpeed(0.005f);
            return;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.EQUALS) && keyBoardState) {
            keyBoardState = false;
            alterSpeed(-0.005f);
            return;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.K) && keyBoardState) {
            keyBoardState = false;
            direction = direction == Direction.down ?  Direction.down : Direction.up;
            return;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.J) && keyBoardState) {
            keyBoardState = false;
            direction = direction == Direction.up?  Direction.up : Direction.down;
            return;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.H) && keyBoardState) {
            keyBoardState = false;
            direction = direction == Direction.right ?  Direction.right : Direction.left;
            return;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.L) && keyBoardState) {
            keyBoardState = false;
            direction = direction == Direction.left ?  Direction.left: Direction.right;
            return;
        }

        gameOver();
        if (count >= speed && direction != Direction.stop && direction != Direction.over) {
            move();
            addTOxyList();
            count = 0;
        }
        keyBoardState = true;
    }

    public void addTOxyList() {
        xyList.add(new int[]{xy[0], xy[1]});
        if (xyList.size() > gold+2) {
            xyList.remove(0);
        }
    }

    public void alterSpeed(float speed) {
        this.speed += speed;
        return;
    }

    public int[] getXy() {
        return xy;
    }

    public void awardedMarks() {
        this.gold += 1;
    }

    public void dispose() {
        texture.dispose();
        pixmap.dispose();
    }
}
