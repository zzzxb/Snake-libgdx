package xyz.zzzxb.snake.utils;

/**
 * @author Zzzxb  2019/10/12 21:46
 * @description:
 */
public enum Direction {
    up,down,left,right,stop,over
}
