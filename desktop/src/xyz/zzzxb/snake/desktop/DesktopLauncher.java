package xyz.zzzxb.snake.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import org.omg.CORBA.VersionSpecHelper;
import xyz.zzzxb.snake.SnakeMain;

public class DesktopLauncher {
    private static final String VERSION = "version : 1.0";

	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Snake " + VERSION;
		config.height = 16*32;
		config.width = 16*32;
		config.resizable = false;
		config.addIcon("favicon.png", Files.FileType.Internal);
		new LwjglApplication(new SnakeMain(), config);
	}
}
